# Base this image on core-image-minimal
include recipes-core/images/core-image-minimal.bb

DESCRIPTION = "Project image"
LICENSE = "MIT"

# SSH server
IMAGE_FEATURES:append = " ssh-server-dropbear"

# Base stuff
IMAGE_INSTALL += " \
                  ${MACHINE_EXTRA_RRECOMMENDS} \
                  kernel-modules \
                  tzdata \
                  dhcpcd \
                  ntp \
                  wpa-supplicant \
                  bluez5 \
                  iw \
                  rfkill \
                "

add_bluetooth_dir() {
  # bluetoothd systemd unit restricts creating directory in /var/lib
  if [ ! -L ${IMAGE_ROOTFS}/var/lib/bluetooth ] &&
    [ ! -e ${IMAGE_ROOTFS}/var/lib/bluetooth ]; then
    mkdir -p ${IMAGE_ROOTFS}/var/lib/bluetooth
    chown root:root ${IMAGE_ROOTFS}/var/lib/bluetooth
  fi
}

ROOTFS_POSTINSTALL_COMMAND +=  "add_bluetooth_dir; "

