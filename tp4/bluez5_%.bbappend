FILES:${PN} += "/data/bluetooth"

do_install:append() {
  install -d ${D}/var/lib/
  rm -rf ${D}/var/lib/bluetooth
  install -d ${D}/data/bluetooth
  ln -sf /data/bluetooth ${D}/var/lib/bluetooth

  # fixes /var/lib/bluetooth issue with bluetooth systemd unit file
  sed -i 's/^StateDirectory=bluetooth/#StateDirectory=bluetooth/' ${D}${systemd_unitdir}/system/bluetooth.service
  sed -i 's/^ProtectSystem=strict/ProtectSystem=full/' ${D}${systemd_unitdir}/system/bluetooth.service
}