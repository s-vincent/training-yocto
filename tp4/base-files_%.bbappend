do_install:append () {
  if [ -z "${@d.getVar('MENDER_ARTIFACT_NAME')}" ] ||
    [ "${@d.getVar('MENDER_ARTIFACT_NAME')}" = "None" ]; then
    cat >> ${D}${sysconfdir}/fstab <<EOF
# Data partition
/dev/mmcblk0p3 /data ext4 defaults 0 0
EOF
  fi
}
