FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI += " file://wpa_supplicant-wlan0.conf"

FILES:${PN} += "${sysconfdir}/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf"
FILES:${PN} += "/data/etc/wpa_supplicant"

SYSTEMD_SERVICE:${PN} = "wpa_supplicant-nl80211@wlan0.service"

do_install:append() {
  install -d ${D}${sysconfdir}/wpa_supplicant
  install -d ${D}/data/etc/wpa_supplicant
  install -m 0755 ${WORKDIR}/wpa_supplicant-wlan0.conf \
    ${D}/data/etc/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf
  ln -sf /data/etc/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf \
    ${D}${sysconfdir}/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf
}