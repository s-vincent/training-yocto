FILES:${PN} += "/data/dropbear"

do_install:append() {
  install -d ${D}/var/lib/
  install -d ${D}/data/dropbear
  rm -rf ${D}/var/lib/dropbear
  rm -rf ${D}${sysconfdir}/dropbear
  ln -sf /data/dropbear ${D}/var/lib/dropbear
  ln -sf /data/dropbear ${D}${sysconfdir}/dropbear
}