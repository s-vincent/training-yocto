# Class to create the "data.ext2"

IMAGE_CMD:rpi-data-image() {
    echo "Creating data partition of ${RPI_DATA_PART_SIZE} MB"
    mkdir -p "${IMAGE_ROOTFS}/data"
    dd if=/dev/zero of="${WORKDIR}/data.ext4" count=0 bs=1M seek=${RPI_DATA_PART_SIZE}
    mkfs.ext4 \
        -d "${IMAGE_ROOTFS}/data" \
        -b 1024 \
        -N 0 \
        -r 1 \
        -m 5 \
        -L data \
        "${WORKDIR}/data.ext4"
    #${RPI_DATA_PART_SIZE}

    install -m 0644 "${WORKDIR}/data.ext4" "${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.data.img"
}

# We need the data contents intact.
do_image_rpi-data-image[respect_exclude_path] = "0"