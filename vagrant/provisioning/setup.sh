#!/bin/sh

echo Install basic dependencies
apt-get update && apt-get install -y git docker.io docker-compose curl neofetch pwgen \
  python3-docker curl unattended-upgrades

echo Done!

