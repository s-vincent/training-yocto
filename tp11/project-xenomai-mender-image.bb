# Base this image on project-image
include recipes-core/images/project-xenomai-image.bb

DESCRIPTION = "Project image with Mender and Dovetail kernel + Xenomai test suite"

IMAGE_INSTALL += " \
                  mender-connect \
                  mender-server-certificate \
                 "

