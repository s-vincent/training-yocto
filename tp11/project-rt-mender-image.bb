# Base this image on project-image
include recipes-core/images/project-rt-image.bb

DESCRIPTION = "Project image with Mender and PREEMPT-RT kernel + real-time test suite"

IMAGE_INSTALL += " \
                  mender-server-certificate \
                  mender-connect \
                 "

