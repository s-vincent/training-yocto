SUMMARY = "Hello world module"
DESCRIPTION = "Hello world module"
LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-3.0-only;md5=c79ff39f19dfec6d293b95dea7b07891"
SECTION = "examples"

DEPENDS += "bc-native"
RPROVIDES:${PN} += "kernel-module-hello"

S = "${WORKDIR}/hello-module"

SRC_URI = "file://hello-module.tar.bz2"
SRC_URI[sha256sum] = "65e6354b5f63f2072815489b6e4670a55db434e432e9ab17accf86914313f876  "

inherit module

