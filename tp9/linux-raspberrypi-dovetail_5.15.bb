DESCRIPTION= "Linux Dovetail kernel for Raspberry Pi (for Xenomai)"
SECTION = "kernel"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

require recipes-kernel/linux/linux-raspberrypi.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
LINUX_VERSION ?= "5.15.52"
LINUX_RPI_BRANCH ?= "rpi-5.15.y"
LINUX_RPI_KMETA_BRANCH ?= "yocto-5.15"

SRCREV_machine = "2aae52ee45098062e9f4dd62ea63fdc22e5d58fc"
SRCREV_meta = "263a2fb6fb2ed6f632d8d62fb46be2c51553b662"
KMETA = "kernel-meta"

# need that to make USB driver working with Dovetail kernel
# see https://raspberrypi.stackexchange.com/questions/4090/how-can-dwc-otg-speed-1-be-made-to-work

SRC_URI = " \
    git://github.com/raspberrypi/linux.git;name=machine;branch=${LINUX_RPI_BRANCH};protocol=https \
    git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=${LINUX_RPI_KMETA_BRANCH};destsuffix=${KMETA} \
    https://source.denx.de/Xenomai/xenomai/-/archive/v3.2.4/xenomai-v3.2.4.tar.bz2;name=xeno \
    file://patch-5.15.51-dovetail1-rpi.patch;apply=0 \
    file://0001-audit-node.patch \
    file://0002-vdso.patch \
    file://0003-gcc-13.patch \
    file://fragment.cfg \
    "

SRC_URI[xeno.sha256sum] = "020052d5cf668f26cceb8117ffbc9fd8e8c66151686c088d9e3ce97eb69f8915"

do_prepare_kernel() {
  xenomai_src="${WORKDIR}/xenomai-v3.2.4/"
  echo "Apply Dovetail patch for ${TUNE_ARCH}"
  ${xenomai_src}/scripts/prepare-kernel.sh --arch=${TUNE_ARCH} --linux=${S} \
    --dovetail=${WORKDIR}/patch-5.15.51-dovetail1-rpi.patch --default
}

addtask prepare_kernel after do_kernel_configme before do_configure

RPI_KERNEL_DEVICETREE:arm = " \
    bcm2710-rpi-3-b.dtb \
    bcm2710-rpi-3-b-plus.dtb \
    bcm2710-rpi-zero-2.dtb \
    bcm2711-rpi-4-b.dtb \
    bcm2711-rpi-400.dtb \
    bcm2708-rpi-cm.dtb \
    bcm2710-rpi-cm3.dtb \
    bcm2711-rpi-cm4.dtb \
    "

RPI_KERNEL_DEVICETREE:aarch64 = " \
    broadcom/bcm2710-rpi-3-b.dtb \
    broadcom/bcm2710-rpi-3-b-plus.dtb \
    broadcom/bcm2710-rpi-zero-2.dtb \
    broadcom/bcm2711-rpi-4-b.dtb \
    broadcom/bcm2711-rpi-400.dtb \
    broadcom/bcm2708-rpi-cm.dtb \
    broadcom/bcm2710-rpi-cm3.dtb \
    broadcom/bcm2711-rpi-cm4.dtb \
    "

RPI_KERNEL_DEVICETREE_OVERLAYS:remove = " \
    overlays/hifiberry-amp3.dtbo \
    overlays/hifiberry-amp4pro.dtbo \
    overlays/imx708.dtbo \
    overlays/vc4-kms-v3d-pi5.dtbo \
    "

KERNEL_DTC_FLAGS += "-@ -H epapr"

