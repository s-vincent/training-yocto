SUMMARY = "Xenomai userland binaries and libraries"
DESCRIPTION = "Xenomai userland binaries and libraries"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://include/COPYING;md5=79ed705ccb9481bf9e7026b99f4e2b0e"
SECTION = "real-time"
PR = "r0"

# based on an article from OpenSilicium 16 magazine
SRC_URI = " \
           https://source.denx.de/Xenomai/xenomai/-/archive/v${PV}/xenomai-v${PV}.tar.bz2;name=xeno \
          "

SRC_URI[xeno.sha256sum] = "020052d5cf668f26cceb8117ffbc9fd8e8c66151686c088d9e3ce97eb69f8915"

S = "${WORKDIR}/xenomai-v${PV}"

inherit autotools pkgconfig

includedir = "/usr/include/xenomai"
prefix = "/usr/xenomai"
bindir = "/usr/xenomai/bin"
libdir = "/usr/lib"

# Fixes QA Error
FILES:${PN} += "/dev"
FILES:${PN} += "/usr/xenomai/bin/*"
FILES:${PN} += "/usr/xenomai/sbin/*"
FILES:${PN} += "/usr/xenomai/share/*"
FILES:${PN} += "/usr/lib/*"
FILES:${PN} += "/usr/include/*"
FILES:${PN} += "/usr/share/doc/*"
FILES:${PN} += "/usr/share/man/*"
FILES:${PN} += "/usr/xenomai/demo/*"

PACKAGE_ARCH = "${MACHINE_ARCH}"
EXTRA_OECONF:append = " --with-core=cobalt --enable-smp --enable-pshared"
EXTRA_OEMAKE:append = " 'LDFLAGS=${LDFLAGS}'"

# Adds xeno-config to sysroot
SYSROOT_DIRS += "${bindir}"

