# Base this image on project-image
include recipes-core/images/project-image.bb

python () {
    if d.getVar("PREFERRED_PROVIDER_virtual/kernel") != "linux-raspberrypi-dovetail":
        raise bb.parse.SkipPackage("Set PREFERRED_PROVIDER_virtual/kernel to linux-raspberrypi-dovetail to enable it")
}

DESCRIPTION = "Project image with Dovetail kernel and Xenomai test suite"

DEPENDS += "linux-raspberrypi-dovetail"

IMAGE_INSTALL += " \
                  xenomai \
                  rt-tests \
                 "
