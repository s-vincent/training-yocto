python () {
    if d.getVar("PREFERRED_PROVIDER_virtual/kernel") == "linux-raspberrypi-rt":
        d.prependVar('CMDLINE_ROOTFS',
          'dwc_otg.fiq_enable=0 dwc_otg.fiq_fsm_enable=0 dwc_otg.nak_holdoff=0 ')
    elif d.getVar("PREFERRED_PROVIDER_virtual/kernel") == "linux-raspberrypi-dovetail":
        d.prependVar('CMDLINE_ROOTFS',
          'dwc_otg.fiq_enable=0 dwc_otg.fiq_fsm_enable=0 dwc_otg.nak_holdoff=0 ')
        d.prependVar('CMDLINE_ROOTFS',
          'isolcpus=0,1 xenomai.supported_cpus=0x3 ')
}

