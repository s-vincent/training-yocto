DESCRIPTION= "Linux PREEMPT-RT kernel for Raspberry Pi"
SECTION = "kernel"

require recipes-kernel/linux/linux-raspberrypi.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/linux-raspberrypi-rt:"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

LINUX_VERSION ?= "5.15.92"
LINUX_RPI_BRANCH ?= "rpi-5.15.y"
LINUX_RPI_KMETA_BRANCH ?= "yocto-5.15"

SRCREV_machine = "14b35093ca68bf2c81bbc90aace5007142b40b40"
SRCREV_meta = "509f4b9d68337f103633d48b621c1c9aa0dc975d"

KMETA = "kernel-meta"

SRC_URI = " \
    git://github.com/raspberrypi/linux.git;name=machine;branch=${LINUX_RPI_BRANCH};protocol=https \
    git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=${LINUX_RPI_KMETA_BRANCH};destsuffix=${KMETA} \
    file://patch-5.15.92-rt57.patch \
    "

KERNEL_DTC_FLAGS += "-@ -H epapr"

