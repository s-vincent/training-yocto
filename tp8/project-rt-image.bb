# Base this image on project-image
include recipes-core/images/project-image.bb

python () {
    if d.getVar("PREFERRED_PROVIDER_virtual/kernel") != "linux-raspberrypi-rt":
        raise bb.parse.SkipPackage(
            "Set PREFERRED_PROVIDER_virtual/kernel to linux-raspberrypi-rt to enable it")
}

DESCRIPTION = "Project image with PREEMPT-RT kernel and real-time test suite"

DEPENDS += "linux-raspberrypi-rt"

IMAGE_INSTALL += " \
                  rt-tests \
                  hwlatdetect \
                  stress-ng \
                 "
