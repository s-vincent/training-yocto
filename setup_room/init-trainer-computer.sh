#!/bin/sh

set -e

if [ -e /etc/debian_version ]; then
  sudo apt update
  sudo apt install ansible sshpass -y
elif [ -e /etc/redhat-release ]; then
  sudo yum install ansible-core -y
  sudo yum install ansible-collection-ansible-posix ansible-collection-community-general -y || echo
fi

ssh-keygen -t rsa -f ~/.ssh/id_rsa -N "" -q

