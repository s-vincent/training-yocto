#!/bin/sh

UPLOAD_PATH=""

# arguments parsing
while [ "$#" -gt 0 ]; do
  case "$1" in
    -u)
      UPLOAD_PATH=$2
      shift 2
      ;;
    -h)
      echo "$0 [-u path] file"
      exit 1;;
    *)
      break
      ;;
  esac
done

scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -P 5555 "$@" rpi@127.0.0.1:${UPLOAD_PATH}

