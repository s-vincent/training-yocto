#!/bin/sh

if [ -z "$BBPATH" ]
then
  echo "Yocto not initialized!"
  exit 1
fi

IMGPATH="$BBPATH/tmp/deploy/images/raspberrypi3-64"
KERNEL_IMAGE=Image
#CMDLINE="rw console=tty1 console=ttyAMA0,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootfstype=ext4 rootwait"
#CMDLINE="rw console=tty1 console=ttyAMA0,115200 $(cat ${IMGPATH}/bootfiles/cmdline.txt | sed 's/console=serial0,115200//')"
CMDLINE="rw console=tty0 console=tty1 console=ttyAMA0,115200 $(cat ${IMGPATH}/bootfiles/cmdline.txt)"
OPTDISPLAY="-display sdl,grab-mod=rctrl"

echo $CMDLINE

# arguments parsing
while [ "$#" -gt 0 ]; do
  case "$1" in
    -u)
      # boot U-boot image
      KERNEL_IMAGE=u-boot.bin
      shift 1
      ;;
    -d)
      OPTDISPLAY="-display none"
      shift 1
      ;;
    -h)
      echo "$0 [-u] [-d]"
      exit 1
      ;;
    *)
      break
      ;;
  esac
done

SDIMG_PATHS=$(ls ${IMGPATH}/$1*sdimg)
SDIMG_PATH=""

for IMG in ${SDIMG_PATHS}
do
  if [ -L "${IMG}" ]; then
    SDIMG_PATH="${IMG}"
    break
  fi
done

if [ -z ${SDIMG_PATH} ]; then
  echo "No image found!"
  exit 1
fi

# if need more, change 4G to what you want!
if [ $(wc -c ${SDIMG_PATH} | awk '{ print $1}') -lt 4294967296 ]; then
  qemu-img resize ${SDIMG_PATH} 4G
fi

qemu-system-aarch64 \
    -M raspi3b \
    -cpu cortex-a53 \
    -m 1G -smp 4 \
    -dtb $IMGPATH/bcm2710-rpi-3-b-plus.dtb \
    -kernel $IMGPATH/${KERNEL_IMAGE} \
    -append "${CMDLINE}" \
    -serial mon:stdio \
    -sd "$SDIMG_PATH" \
    ${OPTDISPLAY} \
    -usb -device usb-mouse -device usb-kbd \
    -device usb-net,netdev=net0 \
    -netdev user,id=net0,hostfwd=tcp::5555-:22

